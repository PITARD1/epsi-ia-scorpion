from utils import *


import matplotlib as mpl
mpl.use("TkAgg");
import matplotlib.pyplot


import numpy as np

# definition de la population
pop = create_population()
generation=0

#création des graphes
plot = []
plot2= []
plot3 = []
plot4 = []
	print("GENARATIUON N : " + str(generation))
	total_score, resultat, total_puissance, variance, distance = fonction_fitness_total(pop)
	new_pop = selection(pop)

	pop = []
	for couple in new_pop:
		new_individu1, new_individu2 = crossover(couple)
		pop.append(mutation(new_individu1))
		pop.append(mutation(new_individu2))
	generation+=1
	# assignement des valeurs dans lse graphes
	plot.append(total_score / nb_pop)
	plot2.append(total_puissance/nb_pop)
	plot3.append(np.var(variance))
	plot4.append(distance/nb_pop)

# affiche des graphes.
mpl.pyplot.subplot(221)
mpl.pyplot.title('Moyenne score')
mpl.pyplot.plot(plot)
mpl.pyplot.subplot(222)
mpl.pyplot.title('Puissance Impact')
mpl.pyplot.plot(plot2)
mpl.pyplot.subplot(223)
mpl.pyplot.title('Variance')
mpl.pyplot.plot(plot3)
mpl.pyplot.subplot(224)
mpl.pyplot.title('Distance tir')
mpl.pyplot.plot(plot4)
mpl.pyplot.show()
