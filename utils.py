import math
import random


# on la choisi pour que se soit cohérent
distance = 300
# defini dans quelle cas on tire une nouvelle hauteur de coupe
taux_varation_coupe = 60
# taux de mutation comris entre 0.1 et 10%
taux_mutation = 1

# taille de la population
nb_gene = 6

# variables des matériaux
p = 800
v = 0.24
E = 20
g = 0.0981

# fonction de création de la population
def create_population():
	population = []
	for i in range(0,nb_pop):
		population.append(create_individu())
	return population

# fonction de création d'un individu
def create_individu() :
	inividu = {
	"a" : random.randint(0,90),
	"lb" : random.randint(1,50),
	"b" : random.uniform(0.05,0.80),
	"bf" : random.uniform(0.05,0.80),
	"h" : random.uniform(0.05,0.80),
	"lc" : random.randint(1,50),
	"lf" : random.randint(1,50)
	}
	return inividu

# permet de calculer les différent chiffre clé pour les graphs
def fonction_fitness_total (population): 
	total_score = 0
	total_energie_impact = 0
	total_distance =0
	all_score = []
	resultat =[]
	for i in range(0,nb_pop):
		result = fonction_fitness(population[i])
		total_score += result["score"]
		total_energie_impact += result["Energie_impact"]
		all_score.append(result["score"])

		total_distance+=result["distance"]
		resultat.append(result)
	return total_score, resultat, total_energie_impact, all_score, total_distance


# fonction qui calcul et donne le résultat de l'individu en fonction de ses gênes.
def fonction_fitness (individu) :
	a = individu["a"]
	lb = individu["lb"]
	b = individu["b"]
	bf = individu["bf"]
	h = individu["h"]
	lc = individu["lc"]
	lf = individu["lf"]
	try:
		K = (1/3)*E/(1-2*v)
		#print("K = " + str(K))
		lv = (1/2)*math.sqrt(math.pow(lb, 2)*math.pow(lc, 2))
		#print("lv = " + str(lv))
		ld = lf - lv
		#print("ld = " + str(ld))
		MP = p*bf*h*lf
		#print("MP = " + str(MP))
		velocite = math.sqrt((K*math.pow(ld, 2))/MP)
		#print("Velocite = " + str(Velocite))

		Ec = (1/2)*MP*math.pow(velocite, 2)
		#print("Ec = " + str(Ec))
		TNT = Ec/4184
		#print("TNT = " + str(TNT))

		d =math.pow(velocite, 2)/g * math.sin( math.radians(a))

		if lv > lf or lc > lb  :
			Ec = 0
			TNT = 0
			d =  0.00000000000001
		result = {
		"Energie_impact": Ec,
		"TNT":  TNT,
		"distance" : d,
		"score" : 1/(abs(350-d)+1) + 1/(abs(100000000-TNT)+1)
		}
		# print("score = " + str(1/(abs(350-d)+1)))
	except Exception :
		result = {
		"Energie_impact": 0,
		"TNT":  0,
		"distance" : 0.00000000000001,
		"score" : 0.00000001
		}
		# print("porté = 0")
	return result

# fonction de mutation d'un individu
# sur chaque individu tirer un nombre aléatoire, si < taux de mutation on change un gène aléatoirement
# avec une nouvelle valeurs généré comme lors de la création de la population
def mutation (individu) :
	mut = random.randint(0,100)
	if mut < taux_mutation:
		# on fait une mutation
		no_gene = random.randint(0,5)
		individu[list(individu.keys())[no_gene]] = random.randint(0,100)
	return individu

# sur une population, tire aléatoirament 15 individu et les compare
# le plus fort est retourné.
def tournament(pop, k):
  best = None
  for i in range(1,k):
      ind = pop[random.randint(0, len(pop)-1)]
      if (best == None) or fonction_fitness(ind)["score"] > fonction_fitness(best)["score"]:
          best = ind
  return best

# selectionne les meilleurs individu deux par deux pour que il puissent faire des enfants.
def selection(pop):
  parents = []
  while len(parents)!=nb_pop/2:
    couple = []
    while len(couple) != 2:
      parent = tournament(pop, 15)
      if len(couple) == 1 :
		  # compare les deux individu pour pas avoir deux fois le même
        if len(couple[0].items() ^ parent.items()) > 0 :
          couple.append(parent)
      else :
        couple.append(parent)
    parents.append(couple)
  return parents

# croise les genes de deux individu
# il en ressort deux autres avec les genes des parents mélangé
def crossover (couple) :
	parent1 = couple[0]
	parent2 = couple[1]

	# définition de la hauteur de coupe, 50 par default
	nb_aleatoire = random.randint(0,100)
	if nb_aleatoire < taux_varation_coupe:
		hauteur_coupe = random.randint(0,100)
	else:
		hauteur_coupe = 50
	coupe = math.floor(len(couple[0]) * (hauteur_coupe / 100))

	# selection des genes qui devront être modifiés
	keys = list(parent1.keys())
	keys_change = keys[- coupe: len(keys)]

	# répartition des genes parents dans les deux anfants.
	enfant1= {}
	enfant2 ={}
	for key in keys:
		if key in keys_change:
			enfant1[key] = parent1[key]
			enfant2[key] = parent2[key]
		if key not in keys_change:
			enfant1[key] = parent2[key]
			enfant2[key] = parent1[key]

	return [enfant1, enfant2]
